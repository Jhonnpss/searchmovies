<?php

namespace SEARCHMOVIES;

require_once __DIR__ . '/../vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Twig\Loader\FilesystemLoader;
use Twig\Environment;
use SEARCHMOVIES\Util\Sessao;
use SEARCHMOVIES\Controllers\ControladorUsuarios;
use Whoops\Run;
use Whoops\Handler\PrettyPageHandler;

$sessao = new Sessao();
$sessao->start();

$loader = new FilesystemLoader('../src/View/');
$twig = new Environment($loader);


include 'rotas.php';

$whoops = new \Whoops\Run;
$whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler);
$whoops->register();

$response = new Response();

$request = new Request();
$request = Request::createFromGlobals();

$contexto = new RequestContext();
$contexto->fromRequest($request);

$matcher = new UrlMatcher($rotas, $contexto);

try {
    $atributos = $matcher->match($contexto->getPathInfo());
    if (empty($atributos)) {
        print_r("erro");
    } else {
        $controller = $atributos['_controller'];
        $method = $atributos['method'];
        if (isset($atributos['suffix'])){
            $parametros = $atributos['suffix'];
        } else {
            $parametros = '';
        }
        $obj = new $controller($response, $request, $twig, $sessao);
        $obj->$method($parametros);
    }
} catch (Exception $ex) {
    $response->setContent('Não encontrado: ', Response::HTTP_NOT_FOUND);
}

$response->send();

