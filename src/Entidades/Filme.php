<?php

namespace SEARCHMOVIES\Entidades;


class Filme {
  
    private $nome;
    private $elenco;
    private $sinopse;
    private $genero;
    private $nacionalidade;
    private $direcao;
    private $dataLancamento;
    private $trailer;
    private $imagem;
    private $ano;
    private $mes;
    private $dia;
    
    function __construct($nome, $elenco, $sinopse, $genero, $nacionalidade, $direcao, $dataLancamento, $trailer, $imagem, $ano, $mes, $dia) {
        $this->nome = $nome;
        $this->elenco = $elenco;
        $this->sinopse = $sinopse;
        $this->genero = $genero;
        $this->nacionalidade = $nacionalidade;
        $this->direcao = $direcao;
        $this->dataLancamento = $dataLancamento;
        $this->trailer = $trailer;
        $this->imagem = $imagem;
        $this->ano = $ano;
        $this->mes = $mes;
        $this->dia = $dia;
    }

    
    function getNome() {
        return $this->nome;
    }

    function getElenco() {
        return $this->elenco;
    }

    function getSinopse() {
        return $this->sinopse;
    }

    function getGenero() {
        return $this->genero;
    }

    function getNacionalidade() {
        return $this->nacionalidade;
    }

    function getDirecao() {
        return $this->direcao;
    }

    function getDataLancamento() {
        return $this->dataLancamento;
    }

    function getTrailer() {
        return $this->trailer;
    }

    function getImagem() {
        return $this->imagem;
    }

    function setNome($nome) {
        $this->nome = $nome;
    }

    function setElenco($elenco) {
        $this->elenco = $elenco;
    }

    function setSinopse($sinopse) {
        $this->sinopse = $sinopse;
    }

    function setGenero($genero) {
        $this->genero = $genero;
    }

    function setNacionalidade($nacionalidade) {
        $this->nacionalidade = $nacionalidade;
    }

    function setDirecao($direcao) {
        $this->direcao = $direcao;
    }

    function setDataLancamento($dataLancamento) {
        $this->dataLancamento = $dataLancamento;
    }

    function setTrailer($trailer) {
        $this->trailer = $trailer;
    }

    function setImagem($imagem) {
        $this->imagem = $imagem;
    }


    function getAno() {
        return $this->ano;
    }

    function getMes() {
        return $this->mes;
    }

    function getDia() {
        return $this->dia;
    }

    function setAno($ano) {
        $this->ano = $ano;
    }

    function setMes($mes) {
        $this->mes = $mes;
    }

    function setDia($dia) {
        $this->dia = $dia;
    }



}
