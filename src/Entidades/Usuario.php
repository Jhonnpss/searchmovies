<?php

namespace SEARCHMOVIES\Entidades;

class Usuario {
    
    private $id;
    private $nome;
    private $login;
    private $senha;
    
    function __construct($nome, $login, $senha) {
        $this->nome = $nome;
        $this->login = $login;
        $this->senha = $senha;
    }
    
    function getId() {
        return $this->id;
    }

    function getNome() {
        return $this->nome;
    }

    function getLogin() {
        return $this->login;
    }

    function getSenha() {
        return $this->senha;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setNome($nome) {
        $this->nome = $nome;
    }

    function setLogin($login) {
        $this->login = $login;
    }

    function setSenha($senha) {
        $this->senha = $senha;
    }


  
}
