<?php
namespace SEARCHMOVIES\Modelos;

use SEARCHMOVIES\Entidades\Filme;
use SEARCHMOVIES\Util\Conexao;
use PDO;


class ModelsFilmes {
    
    public function insertFilmes(Filme $filme){
        try{
            $sql = 'insert into filme (nome, elenco, sinopse, genero, nacionalidade, direcao, dataLancamento, trailer, imagem, ano, mes, dia) '
                    . 'values (:n , :e, :s, :g, :nc, :dr, :dl, :t, :i, :a, :m, :d)';
            $p_sql = Conexao::getInstancia()->prepare($sql);
            $p_sql->bindValue(':n', $filme->getNome());
            $p_sql->bindValue(':e', $filme->getElenco());
            $p_sql->bindValue(':s', $filme->getSinopse());
            $p_sql->bindValue(':g', $filme->getGenero());
            $p_sql->bindValue(':nc', $filme->getNacionalidade());
            $p_sql->bindValue(':dr', $filme->getDirecao());
            $p_sql->bindValue(':dl', $filme->getDataLancamento());
            $p_sql->bindValue(':t', $filme->getTrailer());
            $p_sql->bindValue(':i', $filme->getImagem());
            $p_sql->bindValue(':a', $filme->getAno());
            $p_sql->bindValue(':m', $filme->getMes());
            $p_sql->bindValue(':d', $filme->getDia());
            $p_sql->execute();
            return true;
        } catch (Exception $ex) {
            print_r("Deu ruim de buscar no banco!!!");
        }
    }
    
    public function alterFilmes(Filme $filme, $id){
        try{
            $sql = 'UPDATE filme SET nome = :n, elenco = :e, sinopse = :s, genero = :g, nacionalidade = :nc, direcao = :dr, dataLancamento = :dl, trailer = :t, imagem = :i, ano = :a, mes = :m, dia = :d where id = :id ';
            $p_sql = Conexao::getInstancia()->prepare($sql);
            $p_sql->bindValue(':id', $id);
            $p_sql->bindValue(':n', $filme->getNome());
            $p_sql->bindValue(':e', $filme->getElenco());
            $p_sql->bindValue(':s', $filme->getSinopse());
            $p_sql->bindValue(':g', $filme->getGenero());
            $p_sql->bindValue(':nc', $filme->getNacionalidade());
            $p_sql->bindValue(':dr', $filme->getDirecao());
            $p_sql->bindValue(':dl', $filme->getDataLancamento());
            $p_sql->bindValue(':t', $filme->getTrailer());
            $p_sql->bindValue(':i', $filme->getImagem());
            $p_sql->bindValue(':a', $filme->getAno());
            $p_sql->bindValue(':m', $filme->getMes());
            $p_sql->bindValue(':d', $filme->getDia());
            $p_sql->execute();
            return true;
        } catch (Exception $ex) {
            print_r("Deu ruim de buscar no banco!!!");
        }
    }


    public function buscarFilme($nome) {
        try {
            $sql = 'SELECT * FROM filme where nome = :n';
            $p_sql = Conexao::getInstancia()->prepare($sql);
            $p_sql->bindValue(':n',$nome);
            $p_sql->execute();
            $p_sql->setFetchMode(PDO::FETCH_OBJ);
            return $p_sql->fetch();
        } catch (Exception $exc) {
            print_r("Deu ruim de buscar no banco!!!");
        }
    }
    
    public function buscarFilmeId($id) {
        try {
            $sql = 'SELECT * FROM filme where id = :i';
            $p_sql = Conexao::getInstancia()->prepare($sql);
            $p_sql->bindValue(':i',$id);
            $p_sql->execute();
            $p_sql->setFetchMode(PDO::FETCH_OBJ);
            return $p_sql->fetch();
        } catch (Exception $exc) {
            print_r("Deu ruim de buscar no banco!!!");
        }
    }
    
    public function listarFilmes(){
        try{
            $sql = 'SELECT * FROM filme';
            $p_sql = Conexao::getInstancia()->prepare($sql);
            $p_sql->execute();
            $p_sql->setFetchMode(PDO::FETCH_OBJ);
            return $p_sql->fetchAll();
        } catch (Exception $ex) {
            print_r("Deu ruim de buscar no banco!!!");
        }
    }
    
    public function listarFilmesEspecificos($nome){
        try{
            $sql = 'SELECT * FROM filme where nome = :nome ';
            $p_sql = Conexao::getInstancia()->prepare($sql);
            $p_sql->bindValue(':nome',$nome);
            $p_sql->execute();
            $p_sql->setFetchMode(PDO::FETCH_OBJ);
            return $p_sql->fetchAll();
        } catch (Exception $ex) {
            print_r("Deu ruim de buscar no banco!!!");
        }
    }
    
    public function existeFilme($nome){
        try{
            $sql = 'SELECT * FROM filme where nome = :n';
            $p_sql = Conexao::getInstancia()->prepare($sql);
            $p_sql->bindValue(':n',$nome);
            $p_sql->execute();
            return $count = $p_sql->rowCount();
        } catch (Exception $ex) {
            print_r("Deu ruim de buscar no banco!!!");
        }
    }
    
    public function deletaFilme($id){
        try{
            $sql = 'DELETE FROM filme where id = :id';
            $p_sql = Conexao::getInstancia()->prepare($sql);
            $p_sql->bindValue(':id',$id);
            $p_sql->execute();
            return $count = $p_sql->rowCount();
        } catch (Exception $ex) {
            print_r("Deu ruim de buscar no banco!!!");
        }
       
    }
    
}
