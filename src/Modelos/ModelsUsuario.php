<?php
namespace SEARCHMOVIES\Modelos;

use SEARCHMOVIES\Entidades\Usuario;
use SEARCHMOVIES\Util\Conexao;
use PDO;


class ModelsUsuario {
    
    public function insertUsuario(Usuario $user){
        try{
            $sql = 'insert into usuario (nome, login, senha) values (:n , :l, :s)';
            $p_sql = Conexao::getInstancia()->prepare($sql);
            $p_sql->bindValue(':n', $user->getNome());
            $p_sql->bindValue(':l', $user->getLogin());
            $p_sql->bindValue(':s', $user->getSenha());
            $p_sql->execute();
            return true;
        } catch (Exception $ex) {
            print_r("Deu ruim de buscar no banco!!!");
        }
    }


    public function buscarUsuario($login, $senha) {
        try {
            $sql = 'SELECT id FROM usuario where login = :l AND senha = :s';
            $p_sql = Conexao::getInstancia()->prepare($sql);
            $p_sql->bindValue(':l',$login);
            $p_sql->bindValue(':s',$senha);
            $p_sql->execute();
            $p_sql->setFetchMode(PDO::FETCH_OBJ);
            return $p_sql->fetch();
        } catch (Exception $exc) {
            print_r("Deu ruim de buscar no banco!!!");
        }
    }
    
    public function listarUsuarios(){
        try{
            $sql = 'SELECT * FROM usuario';
            $p_sql = Conexao::getInstancia()->prepare($sql);
            $p_sql->execute();
            $p_sql->setFetchMode(PDO::FETCH_OBJ);
            return $p_sql->fetchAll();
        } catch (Exception $ex) {
            print_r("Deu ruim de buscar no banco!!!");
        }
    }
    
    public function existeUsuario($login){
        try{
            $sql = 'SELECT id FROM usuario where login = :l';
            $p_sql = Conexao::getInstancia()->prepare($sql);
            $p_sql->bindValue(':l',$login);
            $p_sql->execute();
            return $count = $p_sql->rowCount();
        } catch (Exception $ex) {
            print_r("Deu ruim de buscar no banco!!!");
        }
    }
    
    public function existeUsuarioLoginSeha($login, $senha){
        try{
            $sql = 'SELECT id FROM usuario where login = :l AND senha = :s';
            $p_sql = Conexao::getInstancia()->prepare($sql);
            $p_sql->bindValue(':l',$login);
            $p_sql->bindValue(':s',$senha);
            $p_sql->execute();
            return $count = $p_sql->rowCount();
        } catch (Exception $ex) {
            print_r("Deu ruim de buscar no banco!!!");
        }
    }
}
