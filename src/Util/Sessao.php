<?php

namespace SEARCHMOVIES\Util;


class Sessao {
   
    function __construct() {
        
    }
    
    function start(){
        return session_start();
    }
    
    function add($chave, $valor){
        $_SESSION['searchmovies'][$chave] = $valor;
    }
    
    function get($chave){
        if(isset($_SESSION['searchmovies'][$chave]))
            return $_SESSION['searchmovies'][$chave];
        return '';
    }
    
    function rem($chave){
        if(isset($_SESSION['searchmovies'][$chave]))
            session_unset($_SESSION['searchmovies'][$chave]);
    }
    
    function del(){
        if(isset($_SESSION['searchmovies']))
            session_unset($_SESSION['searchmovies']);
        session_destroy();
        
    }
    
    function existe($chave){
        if(isset($_SESSION['searchmovies'][$chave]))
            return true;
        return false;
        
    }
}
