<?php

namespace SEARCHMOVIES\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Twig\Environment;
use SEARCHMOVIES\Util\Sessao;
use SEARCHMOVIES\Modelos\ModelsUsuario;
use SEARCHMOVIES\Modelos\ModelsFilmes;

class ControladorUsuarios {
    
    private $response;
    private $twig;
    private $sessao;
    private $request;
            
    function __construct(Response $response, Request $request, Environment $twig, Sessao $sessao){
     $this->response = $response;
     $this->request = $request;
     $this->twig = $twig;
     $this->sessao = $sessao;
    }
    function ajax($parametro){
        $this->response->setContent('resposta via ajax');
    }

    public function index(){
        if($this->sessao->existe('Login')){
            $this->sessao->del();
        }
        
        $modelsFilmes = new ModelsFilmes();
        $dados = $modelsFilmes->listarFilmes();
        return $this->response->setContent($this->twig->render('index.twig',['filmes' => $dados]));
    }
    
    
    public function show($id){

        $modelo = new ModelsUsuario();
        $usuario = $modelo->buscarUsuario($id);
        return $this->response->setContent($this->twig->render('usuario.html',['usuario'=>$usuario]));  
    }
    
    public function showPaginaLogin(){
        if($this->sessao->existe('Login')){
            $destino = '/homeCRUD';
            $redirecionar = new RedirectResponse($destino);
            $redirecionar->send();
        }else{
            return $this->response->setContent($this->twig->render('login2.twig'));
        }
          
    }
}
