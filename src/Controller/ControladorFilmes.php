<?php

namespace SEARCHMOVIES\Controller;

use SEARCHMOVIES\Entidades\Filme;
use SEARCHMOVIES\Modelos\ModelsFilmes;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Twig\Environment;
use SEARCHMOVIES\Util\Sessao;

class ControladorFilmes {

    private $response;
    private $request;
    private $twig;
    private $session;

    function __construct(Response $response, Request $request, Environment $twig, Sessao $session) {
        $this->response = $response;
        $this->request = $request;
        $this->twig = $twig;
        $this->session = $session;
    }

    public function insertFilmes() {



        if (isset($_POST['nome'])) {

            $nome = addslashes($_POST['nome']);
            $elenco = addslashes($_POST['elenco']);
            $sinopse = addslashes($_POST['sinopse']);
            $genero = addslashes($_POST['genero']);
            $nacionalidade = addslashes($_POST['nacionalidade']);
            $direcao = addslashes($_POST['direcao']);
            $dataLancamento = addslashes($_POST['dataLancamento']);
            $trailer = addslashes($_POST['trailer']);
            $ano = addslashes($_POST['ano']);
            $mes = addslashes($_POST['mes']);
            $dia = addslashes($_POST['dia']);

            if (isset($_FILES['arquivo'])) {

                $extensao = strtolower(substr($_FILES['arquivo']['name'], -5));
                $novo_nome = md5(time()) . $extensao;
                $diretorio = "/var/www/html/searchmovies/public/img/";

                move_uploaded_file($_FILES['arquivo']['tmp_name'], $diretorio . $novo_nome);

                $imagem = '/img/'.$novo_nome;
            }

            if (!empty($nome) && !empty($elenco) && !empty($sinopse) && !empty($genero) &&
                    !empty($nacionalidade) && !empty($direcao) && !empty($dataLancamento) &&
                    !empty($trailer) && !empty($ano) && !empty($mes) && !empty($dia)) {

                $filme = new Filme($nome, $elenco, $sinopse, $genero, $nacionalidade, $direcao, $dataLancamento, $trailer, $imagem, $ano, $mes, $dia);
                $modelsFilme = new ModelsFilmes();
                $qtd = $modelsFilme->existeFilme($nome);
                if ($qtd >= 1) {
                   echo '<script>alert("Filme já cadastrado no sistema!!!");</script>';
                    $qtd = 0;
                } else {
                    if ($id = $modelsFilme->insertFilmes($filme)) {  
                        echo '<script>alert("Filme inserido com sucesso!");</script>';
                      
                    } else {
                         echo '<script>alert("Erro ao enviar ao inserir filme!");</script>';
                    }
                }
            }
        }else{
                echo '<script>alert("Tem algum campo vazio!! Por favor preenche-os!");</script>';
            }
    }

    public function deleteFilmes() {
        $id = $this->request->get('id');
        $modelsFilme = new ModelsFilmes();
        $filme = $modelsFilme->buscarFilmeId($id);
        if ($filme != null) {
            $retorno = $modelsFilme->deletaFilme($id);
            if ($retorno == 1) {
                echo '<script>alert("Filme deletado com sucesso!");</script>';
            } else {
                echo '<script>alert("Erro ao deletar filme!");</script>';
            }
        } else {
            echo '<script>alert("Filme nao encontrado!");</script>';
        }
    }
    
    public function gerarPdf(){
         $modelsFilme = new ModelsFilmes();
         $dados = $modelsFilme->listarFilmes();
         
    }

    public function alterFilmes() {
        
        if (isset($_POST['nome'])) {

            $nome = addslashes($_POST['nome']);
            $elenco = addslashes($_POST['elenco']);
            $sinopse = addslashes($_POST['sinopse']);
            $genero = addslashes($_POST['genero']);
            $nacionalidade = addslashes($_POST['nacionalidade']);
            $direcao = addslashes($_POST['direcao']);
            $dataLancamento = addslashes($_POST['dataLancamento']);
            $trailer = addslashes($_POST['trailer']);
            $ano = addslashes($_POST['ano']);
            $mes = addslashes($_POST['mes']);
            $dia = addslashes($_POST['dia']);
            $id = addslashes($_POST['id']);

            if (isset($_FILES['arquivo'])) {

                $extensao = strtolower(substr($_FILES['arquivo']['name'], -5));
                $novo_nome = md5(time()) . $extensao;
                $diretorio = "/var/www/html/searchmovies/public/img/";

                move_uploaded_file($_FILES['arquivo']['tmp_name'], $diretorio . $novo_nome);

                $imagem = '/img/'.$novo_nome;
            }

            if (!empty($nome) && !empty($elenco) && !empty($sinopse) && !empty($genero) &&
                    !empty($nacionalidade) && !empty($direcao) && !empty($dataLancamento) &&
                    !empty($trailer) && !empty($ano) && !empty($mes) && !empty($dia)) {

                $filme = new Filme($nome, $elenco, $sinopse, $genero, $nacionalidade, $direcao, $dataLancamento, $trailer, $imagem, $ano, $mes, $dia);
                $modelsFilme = new ModelsFilmes();
                $qtd = $modelsFilme->existeFilme($nome);
                if ($qtd >= 1) {
                    echo '<script>alert("Filme já cadastrado no sistema!");</script>';
                    $qtd = 0;
                } else {
                    if ($modelsFilme->alterFilmes($filme, $id)) {
                        echo '<script>alert("Filme alterado com sucesso!");</script>';
                    } else {
                        echo '<script>alert("Error ao alterar o filme");</script>';
                    }
                }
            }else{
                echo '<script>alert("Tem algum campo vazio!! Por favor preenche-os!");</script>';
            }
        }
    }

}
