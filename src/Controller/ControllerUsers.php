<?php

namespace SEARCHMOVIES\Controller;

use SEARCHMOVIES\Controller\ControladorShowCRUD;
use SEARCHMOVIES\Entidades\Usuario;
use SEARCHMOVIES\Modelos\ModelsUsuario;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Twig\Environment;
use SEARCHMOVIES\Util\Sessao;

class ControllerUsers {

    private $response;
    private $request;
    private $twig;
    private $session;

    function __construct(Response $response, Request $request, Environment $twig, Sessao $session) {
        $this->response = $response;
        $this->request = $request;
        $this->twig = $twig;
        $this->session = $session;
    }

    public function inserirUsuario() {

        if (isset($_POST['nome'])) {

            $nome = addslashes($_POST['nome']);
            $login = addslashes($_POST['login']);
            $senha = addslashes($_POST['senha']);
            $confirmaSenha = addslashes($_POST['confirmasenha']);

            if (!empty($nome) && !empty($login) && !empty($senha) && !empty($confirmaSenha)) {

                $u = new Usuario($nome, $login, $senha);
                if ($senha == $confirmaSenha) {

                    $modelsUser = new ModelsUsuario();
                    $qtd = $modelsUser->existeUsuario($u->getLogin());

                    if ($qtd > 0) {
                        print_r ('Ja existe usuario cadastrado!');
                        return false;
                    } else {
                        if ($modelsUser->insertUsuario($u)) {
                            print_r ('Cadastrado com sucesso!');
                        } else {
                            print_r ('Erro na operacao de cadastro!');
                        }
                    }
                } else {
                    echo '<script>alert("Senha e confirmar senha nao ta igual!!!");</script>';
                }
            } else {
                echo 'Preencha todos os campos!';
            }
        }
    }

    public function logar() {
       
        if (isset($_POST['login'])) {
            $login = addslashes($_POST['login']);
            $senha = addslashes($_POST['senha']);

            $modelsUser = new ModelsUsuario();
            $qtd = $modelsUser->existeUsuarioLoginSeha($login, $senha);
            
            if ($qtd > 0) {
                $users = $modelsUser->buscarUsuario($login, $senha);
                $this->session->add('Login', $login);

                $destino = '/homeCRUD';
                $redirecionar = new RedirectResponse($destino);
                $redirecionar->send();

                return true;
            } else {
                echo '<script>alert("Login ou senha incorretos!!!");</script>';
                
                return;
            }
        }
    }
    
    public function logout(){
            
            $this->session->del();
        
            $destino = '/login';
            $redirecionar = new RedirectResponse($destino);
            $redirecionar->send();
        
    }

}
