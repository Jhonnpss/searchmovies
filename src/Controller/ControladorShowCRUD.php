<?php

namespace SEARCHMOVIES\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Twig\Environment;
use SEARCHMOVIES\Util\Sessao;
use SEARCHMOVIES\Modelos\ModelsFilmes;

class ControladorShowCRUD {

    private $response;
    private $twig;
    private $sessao;
    private $request;

    function __construct(Response $response, Request $request, Environment $twig, Sessao $sessao) {
        $this->response = $response;
        $this->request = $request;
        $this->twig = $twig;
        $this->sessao = $sessao;
    }

    public function showPaginaCadastrarFilme() {
        if ($this->sessao->existe('Login')) {
            return $this->response->setContent($this->twig->render('CadastroFilme.twig'));
        } else {
            $destino = '/login';
            $redirecionar = new RedirectResponse($destino);
            $redirecionar->send();
        }
    }

    public function showPaginaAlterarFilme() {
        if ($this->sessao->existe('Login')) {
            $modelsFilmes = new ModelsFilmes();
            $dados = $modelsFilmes->listarFilmes();
            return $this->response->setContent($this->twig->render('alterarFilmes.twig', ['filmes' => $dados]));
        } else {
            $destino = '/login';
            $redirecionar = new RedirectResponse($destino);
            $redirecionar->send();
        }
    }

    public function showPaginaAlterandoFilme() {
        if ($this->sessao->existe('Login')) {
            $id = $this->request->get('id');
            $modelsFilme = new ModelsFilmes();
            $dado = $modelsFilme->buscarFilmeId($id);
            return $this->response->setContent($this->twig->render('alterando.twig',['filme' => $dado]));
        } else {
            $destino = '/login';
            $redirecionar = new RedirectResponse($destino);
            $redirecionar->send();
        }
    }

    public function showDeleteFilmes() {
        
    }

    public function showPaginaCadastraUsuario() {
        return $this->response->setContent($this->twig->render('cadastraUser.twig'));
    }

    public function showPaginaHomeCRUD() {
        if ($this->sessao->existe('Login')) {

            $modelsFilmes = new ModelsFilmes();
            $dados = $modelsFilmes->listarFilmes();
            return $this->response->setContent($this->twig->render('homeCRUD.twig', ['filmes' => $dados]));
        } else {
            $destino = '/login';
            $redirecionar = new RedirectResponse($destino);
            $redirecionar->send();
        }
    }

}
