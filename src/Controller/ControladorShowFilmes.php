<?php

namespace SEARCHMOVIES\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Twig\Environment;
use SEARCHMOVIES\Util\Sessao;
use SEARCHMOVIES\Modelos\ModelsFilmes;


class ControladorShowFilmes {
 
    private $response;
    private $twig;
    private $sessao;
    private $request;
            
    function __construct(Response $response, Request $request, Environment $twig, Sessao $sessao){
     $this->response = $response;
     $this->request = $request;
     $this->twig = $twig;
     $this->sessao = $sessao;
    }
    
    
    
    public function showPaginaPesquisarFilme(){
        $dados = null;
        if (isset($_POST['nome'])) {
        
        $nome = addslashes($_POST['nome']);     
            
        $modelsFilme = new ModelsFilmes();
        $dados = $modelsFilme->listarFilmesEspecificos($nome);    
        
        }
        return $this->response->setContent($this->twig->render('pesquisarFilme.twig',['filmes' => $dados]));
        
        }
    
    public function showFilme(){
        $id = $this->request->get('id');
        $modelsFilme = new ModelsFilmes();
        $dado = $modelsFilme->buscarFilmeId($id);
        return $this->response->setContent($this->twig->render('filme.twig',['filme' => $dado]));
    }
    
    public function paginaSobre(){
        return $this->response->setContent($this->twig->render('sobre.twig'));
    }
    
    
}
