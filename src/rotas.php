<?php

namespace SEARCHMOVIES;

use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;
use SEARCHMOVIES\Controller\ControladorShowCRUD;

$rotas = new RouteCollection();

$rotas->add('index', new Route('/', array('_controller' => '\SEARCHMOVIES\Controller\ControladorUsuarios',
    "method" => 'index', 'suffix' => '')));

$rotas->add('paginaInicial', new Route('/home', array('_controller' => '\SEARCHMOVIES\Controller\ControladorUsuarios',
    "method" => 'index', 'suffix' => '')));

$rotas->add('paginaSobre', new Route('/sobre', array('_controller' => '\SEARCHMOVIES\Controller\ControladorShowFilmes',
    "method" => 'paginaSobre', 'suffix' => '')));

$rotas->add('paginaInicialCRUD', new Route('/homeCRUD', array('_controller' => '\SEARCHMOVIES\Controller\ControladorShowCRUD',
    "method" => 'showPaginaHomeCRUD', 'suffix' => '')));

$rotas->add('paginaLogin', new Route('/login', array('_controller' => '\SEARCHMOVIES\Controller\ControladorUsuarios',
    "method" => 'showPaginaLogin', 'suffix' => '')));

$rotas->add('paginaLogado', new Route('/logado', array('_controller' => '\SEARCHMOVIES\Controller\ControllerUsers',
    "method" => 'logar', 'suffix' => '')));

$rotas->add('logout', new Route('/logout', array('_controller' => '\SEARCHMOVIES\Controller\ControllerUsers',
    "method" => 'logout', 'suffix' => '')));

$rotas->add('filmesporgenero', new Route('/filmesporgenero', array('_controller' => '\SEARCHMOVIES\Controller\ControladorUsuarios',
    "method" => 'showPaginaFilmesGenero', 'suffix' => '')));

$rotas->add('pesquisarFilme', new Route('/pesquisarfilme', array('_controller' => '\SEARCHMOVIES\Controller\ControladorShowFilmes',
    "method" => 'showPaginaPesquisarFilme', 'suffix' => '')));

$rotas->add('cadastraUsuario', new Route('/cadastrausuario', array('_controller' => '\SEARCHMOVIES\Controller\ControladorShowCRUD',
    "method" => 'showPaginaCadastraUsuario', 'suffix' => '')));

$rotas->add('filmespordata', new Route('/filmespordata', array('_controller' => '\SEARCHMOVIES\Controller\ControladorUsuarios',
    "method" => 'showPaginaFilmesData', 'suffix' => '')));

$rotas->add('cadastrarfilmes', new Route('/cadastrarfilmes', array('_controller' => '\SEARCHMOVIES\Controller\ControladorShowCRUD',
    "method" => 'showPaginaCadastrarFilme', 'suffix' => '')));

$rotas->add('alterarfilmes', new Route('/alterarfilmes', array('_controller' => '\SEARCHMOVIES\Controller\ControladorShowCRUD',
    "method" => 'showPaginaAlterarFilme', 'suffix' => '')));

$rotas->add('alterandofilmes', new Route('/alterandofilmes', ['_controller' => '\SEARCHMOVIES\Controller\ControladorShowCRUD'
    ,'method'=> 'showPaginaAlterandoFilme']));

$rotas->add('excluindofilmes', new Route('/excluindofilmes', ['_controller' => '\SEARCHMOVIES\Controller\ControladorFilmes'
    ,'method'=> 'deleteFilmes']));

$rotas->add('filmecadastrado', new Route('/cadastrarfilmes/cadastrado', array('_controller' => '\SEARCHMOVIES\Controller\ControladorFilmes',
    "method" => 'insertFilmes', 'suffix' => '')));

$rotas->add('filmealterado', new Route('/alterarfilmes/alterando', array('_controller' => '\SEARCHMOVIES\Controller\ControladorFilmes',
    "method" => 'alterFilmes', 'suffix' => '')));

$rotas->add('filmespornome', new Route('/filmespornome', array('_controller' => '\SEARCHMOVIES\Controller\ControladorUsuarios',
    "method" => 'showPaginaFilmesNome', 'suffix' => '')));

$rotas->add('usuario', new Route('/usuario/{parametro}', ['controlador' => '\SEARCHMOVIES\Controller\ControladorUsuarios'
    ,'metodo'=> 'show'],['paremetro' => '.*']));

$rotas->add('filme', new Route('/filme', ['_controller' => '\SEARCHMOVIES\Controller\ControladorShowFilmes'
    ,'method'=> 'showFilme']));

$rotas->add('exemplo-ajax', new Route('/ajax/{parametro}', ['controlador' => '\SEARCHMOVIES\Controller\ControladorUsuarios'
    ,'metodo'=> 'ajax']));

$rotas->add('cadastradoUsuario', new Route('/usercadastrado', array('_controller' => '\SEARCHMOVIES\Controller\ControllerUsers',
    "method" => 'inserirUsuario', 'suffix' => '')));

return $rotas;

