
    <!--Import jQuery before materialize.js-->
    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <script type="text/javascript">
        $( document ).ready(function(){
            $(".button-collapse").sideNav();
        });
    </script>
    
    <!--Voltar ao topo-->
	<script type="text/javascript">
        $(document).ready(function(){
        
        //Check to see if the window is top if not then display button
        $(window).scroll(function(){
        if ($(this).scrollTop() > 100) {
            $('.scrollToTop').fadeIn();
        } else {
            $('.scrollToTop').fadeOut();
        }
        });
        
        //Click event to scroll to top
        $('.scrollToTop').click(function(){
        $('html, body').animate({scrollTop : 0},800);
        return false;
        });
        
        });
    </script>