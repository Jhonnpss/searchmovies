$(document).ready(function () {
    $("#formLogin").submit(function (e) {
        e.preventDefault(); 

        var formData = new FormData(this);
        $.ajax({
            type: 'POST',
            url: '/logar',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,

            success: function (dados) {
                $("#div_retorno").html(dados);
            },
            beforeSend: function () {
                $("#processando").css({display: "block"});
            },
            complete: function () {
                $("#processando").css({display: "none"});
            },
            error: function () {
                $("#div_retorno").html("Erro em chamar a função.");
                setTimeout(function () {
                    $("#div_retorno").css({display: "none"});
                }, 5000);
            }
        });
    });
});
